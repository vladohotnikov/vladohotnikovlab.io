﻿This is in-team conventions about unit testing for all backend services.

# Main libraries

* [xUnit](https://xunit.net) - Framework for unit testing
* [Moq](https://documentation.help/Moq) - Isolation framework
* [FluentAssertions](https://fluentassertions.com) - Extension methods that allow you to more naturally specify the expected outcome

# Unit Test basic structure

Use plural postfix for test class name %ClassName%**Tests**

```c#
public class ClassNameTests
{        
    [Fact]
    public void MethodName_StateUnderTest_ExpectedBehavior()
    {
        // test body
    }
}
```

!!! info
    Additional scenarios described in [advanced](advanced.md) section.

## Characteristics of a good unit test

* **Fast.** It is not uncommon for mature projects to have thousands of unit tests. Unit tests should take very little time to run (milliseconds).
* **Isolated.** Unit tests are standalone, can be run in isolation, and have no dependencies on any outside factors such as a file system or database.
* **Repeatable.** Running a unit test should be consistent with its results, that is, it always returns the same result if you do not change anything in between runs.
* **Self-Checking.** The test should be able to automatically detect if it passed or failed without any human interaction.
* **Timely.** A unit test should not take a disproportionately long time to write compared to the code being tested. If you find testing the code taking a large amount of time compared to writing the code, consider a design that is more testable.

## Naming convention

Naming standards are important because they explicitly express the intent of the test. Always follow Microsoft naming conventions. The name of your test should consist of three parts:

1. MethodName - The name of the method being tested.
2. StateUnderTest - The scenario under which it's being tested.
3. ExpectedBehavior - The expected behavior when the scenario is invoked.

```c#
public void MethodName_StateUnderTest_ExpectedBehavior()
{
}
```

!!! info
    Read [Unit Test Naming Conventions article](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices#naming-your-tests) for more info.

## Arrange-Act-Assert

Arrange, Act, Assert is a common pattern when unit testing. As the name implies, it consists of three main actions:

1. **Arrange** - your objects, creating and setting them up as necessary.
2. **Act** - on an object.
3. **Assert** - that something is as expected.

Always split you test on 3 sections.
```c#
public class MyTests
{        
    [Fact]
    public void MethodName_StateUnderTest_ExpectedBehavior()
    {
        // arrange
        
        // act
        
        // assert
    }
}
```
If a single line of code represents two actions, write both of them with `&`
```c#
public class MyTests
{        
    [Fact]
    public void MethodName_StateUnderTest_ExpectedBehavior()
    {
        // arrange & act
        var result = 1 + 1;
        
        // assert
        result.Should().Be(2);
    }
}
```

!!! info
    Read [Arranging Conventions article](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices#arranging-your-tests) for more info.

# Cover convention

When you try to discover what logic should be covered follow these rules:

1. Cover `uncommon logic`.
2. Avoid cover `common logic`.

What does this mean?

## Common logic

`Common logic` - logic provided by external sources.

This should NOT be covered:

* Methods provided external libraries, like:
    * Archive() method of Zip library
    * Send() method of ServiceBus library
    * etc.
* DataBase logic:
    * [CRUD](https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/implementing-basic-crud-functionality-with-the-entity-framework-in-asp-net-mvc-application) operations
    * [LINQ Query Operations](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/basic-linq-query-operations)
    * etc.
* .NET logic like getters, setters, constructor arguments, etc.
* [DTO](https://docs.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-5) - database entities, records and classes without logic
* API Controller methods - because there are should not be any logic

## Uncommon logic

`Uncommon logic` - logic written by us.

This SHOULD be covered:

* Public methods of implementations
* Business logic:
    * `if-else` cases
    * `do-while` sections
    * `try-catch` sections
    * `switch` expressions
    * etc.
* Compile-time logic of external libraries. If it provides validation methods. Like:
    * [Service Provider](index.md#service-provider-unit-testing) (DI-container) configurations
    * [Mapster](index.md#mapster-unit-testing) configurations
* Regular expressions
* Any discovered bugs should be covered by a unit test in the first place

### Service Provider Unit Testing

Each project should contain Service Provider Unit tests. Without it, we can get runtime errors. Example:
```c#
[Fact]
public void BuildServiceProvider_WithDependencies_ShouldNotThrow()
{
    // arrange
    var services = new ServiceCollection();
    var serviceProviderOptions = new ServiceProviderOptions
    {
        ValidateOnBuild = true,
        ValidateScopes = true,
    };

    services.AddYourProjectServices() // add your dependencies here

    // act
    var buildServiceCollection = () =>
    {
        var serviceProvider = services.BuildServiceProvider(serviceProviderOptions);
        serviceProvider.CreateScope();
    };

    // assert
    buildServiceCollection.Should().NotThrow();
}
```

### Mapster Unit Testing

Each project with Mapster should contain its configuration unit tests. Without it, we can get runtime errors. Example:
```c#
[Fact]
public void Compile_WithConfigurations_ShouldNotThrow()
{
    // Arrange
    var config = new TypeAdapterConfig
    {
        RequireExplicitMapping = true,
        RequireDestinationMemberSource = true
    };

    config.Scan(Assembly.GetAssembly(typeof(Program))!); // add your configurations

    // Act
    Action compileAction = () => config.Compile();

    // Assert
    compileAction.Should().NotThrow();
}
```

## GitlabCI (IN PROGRESS)

How we should count code coverage?
How we should write reports?
Cobertura - tool that calculates the percentage of code accessed by tests.
coverlet

# FAQ (IN PROGRESS)

Any questions.

# Links

## Base information

* [xUnit getting started guide](https://xunit.net/docs/getting-started/netcore/visual-studio#write-first-tests)
* [Rider unit test example](https://www.jetbrains.com/help/rider/Getting_Started_with_Unit_Testing.html)
* [Microsoft getting started guide](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test)
* [Coverlet GitHub](https://github.com/coverlet-coverage/coverlet)
* [Cobertura documentation](https://cobertura.github.io/cobertura/)
* [Cobertura github repo](https://github.com/cobertura/cobertura)

## Highly recommended reading

* [Microsoft best practices](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices)
* [xUnit documentation](https://xunit.net/#documentation)
* [Moq quickstart](https://github.com/Moq/moq4/wiki/Quickstart)
* [Moq GitHub repository](https://github.com/moq/moq4)
* [FluentAssertions documentation](https://fluentassertions.com/introduction)
* [FluentAssertions GitHub repository](https://github.com/fluentassertions/fluentassertions)
